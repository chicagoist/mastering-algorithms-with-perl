package Address;

# Create a new address.  Extra arguments are stored in the object:
# $address = new Address(name => "Wolf Blass", country => "Australia" ... )
#
sub new {
    my $package = shift;
    my $self = { @_ };
    return bless $self, $package;
}

# The country() method gets and sets the country field.
#
sub country {
    my $self = shift;
    return @_ ? ($self->{country} = shift) : $self->{country};
}

# The methods for zone, city, street, and name (not shown here)
# will resemble country().

# The as_string() method
sub as_string {
    my $self = shift;
    my $string;

    foreach (qw(name street city zone country)) {
        $string .= "$self->{$_}\n" if defined $self->{$_};
    }

    return $string;
}
